# Code for Tots Quintela, Punzalan, Jaudines
---
#PROJECT TITLE
iBASKETBALL

---
#INTRODUCTION
The project iBASKETBALL is an interactive mobile application which caters to young children. The application teaches them how to play basketball, how basketball works, and mainly it teachers them the scoring system of the mobile application. It's main objective is to help children understand the basics of basketball as a sport.

---
#TEST CASES
The following are the test cases of the program:

Scenario #1: Click +3 points to either teams
Expected Output: The application adds plus three (3) points on either of the team's total score.
Screenshot:

[scenario1.jpg](scenario1.jpg)

Scenario #2: Click +2 points to either teams
Expected Output: The application adds plus two (2) points on either of the team's total score.
Screenshot:

[scenario2.jpg](screenshots/scenario2.jpg)

Scenario #3: Click Free Throw to either teams
Expected Output: The application adds plus one (1) points on either of the team's total score.
Screenshot:

[scenario3.jpg](screenshots/scenario3.jpg)

Scenario #4: Click Reset
Expected Output: The application resets the acquired total scores on either teams.
Screenshot:

[scenario4.jpg](screenshots/scenario4.jpg)
